var gulp        = require('gulp');
var sass        = require('gulp-sass');
var watch       = require('gulp-watch');
var concat      = require('gulp-concat');
var cssnano     = require('gulp-cssnano');

var destpath = './web/assets';
var bootstrapcss = './node_modules/bootstrap/dist/css/*.min.css';
var bootstrapjs = ['./node_modules/jquery/dist/jquery.min.js','./node_modules/bootstrap/dist/js/bootstrap.bundle.js'];
var mainsass = destpath + '/sass/main.scss';
var alljs = './scripts/*.js';

var watchpath = [destpath + '/**/*.scss', alljs];

/**
 * Compile and Bootstrap css files
 */
gulp.task('vendorcss', function() {
  return gulp.src(bootstrapcss)
            .pipe(cssnano())
            .pipe(concat('bootstrap.css'))
            .pipe(gulp.dest(destpath + '/css/'))
});

/**
 * Compile Main custom Sass
 */
gulp.task('sass', function(){
  return gulp.src(mainsass)
            .pipe(sass())
            .pipe(cssnano())
            .pipe(gulp.dest(destpath + '/css/'))
});

/**
* Compile the Sass as readable
*/
gulp.task('sass-readable', function(){
  return gulp.src(mainsass)
            .pipe(sass())
            .pipe(gulp.dest(destpath + '/css/'))
});

/**
 * Basic compilation of Jquery and Bootstrap
 */
gulp.task('vendorjs', function() {
  return gulp.src(bootstrapjs)
            .pipe(concat('bootstrap.js'))
            .pipe(gulp.dest(destpath + '/js/'))
});

/**
 * Basic compilation of all custom js
 */
gulp.task('js', function() {
  return gulp.src(alljs)
            .pipe(concat('main.js'))
            .pipe(gulp.dest(destpath + '/js/'))
});

gulp.task('watch', function () {
  gulp.watch(watchpath, gulp.series('sass'));
});

/*
gulp.task('js', function(){});
*/

gulp.task('default', gulp.series( ['vendorcss', 'sass', 'vendorjs', 'js', 'watch'] ) );