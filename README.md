# README #

### What is this repository for? ###

Just a basic Bootstrap frontend starter

### How do I get set up? ###

* run 'npm install' to install all required libraries
* run 'npm start' to compile all js, sass, etc and begin the watch process

There are also gulp tasks for specific compiling

* 'gulp sass' for just sass files
* 'gulp sass-readable' for non-minified css
* 'gulp js' for bundling up all your js files
* 'gulp watch' to start watching for js or sass changes

